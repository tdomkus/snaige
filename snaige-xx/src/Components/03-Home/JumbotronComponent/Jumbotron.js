import React from "react";

var HomeJumbotronPresentation = () => {
  return (
    <div className="jumbotron inverse">
      <h1 className="display-4">Sveiki atvyke!</h1>
      <p className="lead">Paprasta parduotuve paprastiems zmonems.</p>
      <hr className="my-4" />
      <p>This shop is made using react, bootstrap and axios at the moment.</p>
    </div>
  );
};

export default HomeJumbotronPresentation;
