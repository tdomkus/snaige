package lt.tdomkus;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import lt.tdomkus.cart.model.CartProduct;

@Configuration
@ImportResource({ "classpath*:application-context.xml" })
public class AppConfig {

	@Bean
	public CartProduct cartProduct() {
		return new CartProduct();
	}

}
