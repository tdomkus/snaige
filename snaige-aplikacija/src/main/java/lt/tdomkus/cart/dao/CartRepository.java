package lt.tdomkus.cart.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lt.tdomkus.cart.model.Cart;

public interface CartRepository extends JpaRepository<Cart, Long> {
	Cart findByUsernameIgnoreCase(String username);
}
